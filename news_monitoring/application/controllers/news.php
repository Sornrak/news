<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model','news_today');
	}

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('news_view');
	}

	public function ajax_list()
	{
		$list = $this->news_today->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $news_today) {
			$no++;
			$row = array();
			$row[] = $news_today->day;
			$row[] = $news_today->day_t;

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_news_today('."'".$news_today->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_news_today('."'".$news_today->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->news_today->count_all(),
						"recordsFiltered" => $this->news_today->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->news_today->get_by_id($id);
		$data->day_w = ($data->day == '0000-00-00') ? '' : $data->day; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->_validate();
		$data = array(
				'day' => $this->input->post('day'),
				'day_t' => $this->input->post('day_t'),
			);
		$insert = $this->news_today->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
				'day' => $this->input->post('day'),
				'day_t' => $this->input->post('day_t'),
			);
		$this->news_today->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->news_today->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('day') == '')
		{
			$data['inputerror'][] = 'day';
			$data['error_string'][] = 'กรุณาเลือกวันที่';
			$data['status'] = FALSE;
		}

		if($this->input->post('day_t') == '')
		{
			$data['inputerror'][] = 'day_t';
			$data['error_string'][] = 'กรุณกรอกวันที่ตัวเต็มเป็นภาษาไทย';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}
